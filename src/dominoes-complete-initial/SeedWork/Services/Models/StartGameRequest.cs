﻿using Hqv.Dominoes.Initial.Domain.Models;
using System.Collections.Generic;

namespace Hqv.Dominoes.Initial.Core
{
    public class StartGameRequest
    {
        public StartGameRequest(IEnumerable<Player> players)
        {
            Players = players;
        }

        public IEnumerable<Player> Players { get; }
    }
}
﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Hqv.Dominoes.WebApi.Middlewares
{
    public class ResponseHeaderMiddleware
    {
        private readonly RequestDelegate _next;

        public ResponseHeaderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var correlationId = context.Request.Headers[ApiHeaders.CorrelationId].ToString();
            if (!string.IsNullOrEmpty(correlationId))
            {
                context.Response.Headers.Add(ApiHeaders.CorrelationId, correlationId);
            }

            await _next(context).ConfigureAwait(false);
        }
    }
}


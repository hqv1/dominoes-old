﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hqv.Dominoes.WebApi.Middlewares
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LoggingMiddleware> logger;

        public LoggingMiddleware(RequestDelegate next, ILogger<LoggingMiddleware> logger)
        {
            _next = next;
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var correlationId = context.Request.Headers[ApiHeaders.CorrelationId].ToString();

            if (string.IsNullOrEmpty(correlationId))
            {
                await _next(context).ConfigureAwait(false);
            }
            else
            {
                using (logger.BeginScope(new Dictionary<string, object> { ["CorrelationId"] = correlationId}))
                {
                    await _next(context).ConfigureAwait(false);
                }
            }
        }
    }
}


﻿using FluentValidation;

namespace Hqv.Dominoes.WebApi.Models
{
    public class AddPlayerModelValidator : AbstractValidator<AddPlayerModel>
    {
        public AddPlayerModelValidator()
        {
            RuleFor(x => x.Nickname).NotEmpty();
        }
    }
}

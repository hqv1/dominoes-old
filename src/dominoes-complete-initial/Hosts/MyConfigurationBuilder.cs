﻿using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.AzureKeyVault;
using Serilog;
using Serilog.Exceptions;

namespace Hqv.Dominoes.Hosts
{
    /// <summary>
    /// My Configuration Builder creates an IConfigurationRoot
    /// </summary>
    public static class MyConfigurationBuilder
    {
        private const string appSettingsFileName = "appsettings.json";
        private const string keyVaultFieldName = "KeyVaultName";

        public static ILogger GetLogger(IConfiguration configuration)
        {
            //todo: make a singleton
            return new LoggerConfiguration()
               .ReadFrom.Configuration(configuration)
               .Enrich.WithExceptionDetails()
               .Enrich.FromLogContext()
               .CreateLogger();
        }

        public static IConfigurationRoot GetConfiguration()
        {
            return GetConfigurationBuilder().Build();
        }

        public static IConfigurationBuilder GetConfigurationBuilder()
        {
            var configBuilder = new ConfigurationBuilder()
               .AddJsonFile(appSettingsFileName, optional: false, reloadOnChange: true);
            AddToConfigurationBuilder(configBuilder);
            return configBuilder;
        }

        public static void AddToConfigurationBuilder(IConfigurationBuilder configBuilder)
        {
            var builtConfig = configBuilder.Build();
            var keyVaultName = builtConfig[keyVaultFieldName];

            var logger = GetLogger(builtConfig);

            if (!string.IsNullOrEmpty(keyVaultName))
            {
                //todo: exception handling
                //todo: add environment variable to builder precedence
                //todo: add app settings dev file to builder precedence                
                logger.Debug("Using Key Vault {Name}", keyVaultName);

                KeyVaultClient keyVaultClient;

                // Access Token is needed in cases where Managed Identity doesn't work.
                var accessToken = builtConfig["AZURE_KEY_VAULT_ACCESS_TOKEN"];
                if (!string.IsNullOrEmpty(accessToken))
                {
                    logger.Debug("Using Access Token from Environment variable to access Key Vault");
                    keyVaultClient = new KeyVaultClient( async (string _, string __, string ___) => accessToken);
                }
                else
                {
                    logger.Debug("Using Azure Managed identity to access Key Vault");
                    var azureServiceTokenProvider = new AzureServiceTokenProvider();
                    keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));
                }
                
                // Config builder precedence is last in, so we're adding appsettings at the end so it takes precedence over all other builders.
                configBuilder
                   .AddAzureKeyVault($"https://{keyVaultName}.vault.azure.net/",
                        keyVaultClient,
                        new DefaultKeyVaultSecretManager())
                   .AddJsonFile(appSettingsFileName, optional: false, reloadOnChange: true);

                logger.Information("Key vault {Name} successfully loaded", keyVaultName);
            }
            else
            {
                logger.Information("No Key Vault name in config. Skipping.");
            }
        }
    }
}

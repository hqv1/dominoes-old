﻿using Hqv.Dominoes.Initial.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Hqv.Dominoes.Initial.Data
{
    public class DominoesDbContext : DbContext
    {
        public DominoesDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Player> Players { get; set; } = null!;


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>")]
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if(modelBuilder == null)
            {
                throw new Exception($"{nameof(modelBuilder)} cannot be null");
            }

            modelBuilder.Entity<Player>(
                x =>
                {
                    x.HasKey(p => p.Id);
                    x.Property(p => p.Nickname);
                    x.HasIndex(p => p.Nickname).IsUnique();
                }
                );
        }
    }
}
﻿using AutoMapper;
using Hqv.Dominoes.Hosts;
using Hqv.Dominoes.Initial.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Exceptions;
using System;
using System.Reflection;

namespace Hqv.Dominoes.Initial.ConsoleHost
{
    public static class Ioc
    {
        public static ILogger GetLogger(IConfiguration configuration)
        {
            //todo: make a singleton
            return new LoggerConfiguration()
               .ReadFrom.Configuration(configuration)
               .Enrich.WithExceptionDetails()
               .Enrich.FromLogContext()
               .CreateLogger();
        }

        public static IServiceProvider RegisterComponents(IConfiguration configuration)
        {
            IServiceCollection services = new ServiceCollection();

            MyServiceBuilder.ConfigureServices(configuration, services);

            //var logger = GetLogger(configuration);
            //services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(logger, dispose: true));

            //services.AddOptions();

            //services.AddDbContext<DominoesDbContext>(options =>
            //    options.UseNpgsql(configuration.GetConnectionString("dominoes")));

            //services.AddMediatR(new[] { Assembly.GetExecutingAssembly(), Assembly.GetAssembly(typeof(Core.Handlers.AddPlayerHandler)) });

            //services.AddAutoMapper(Assembly.GetAssembly(typeof(Core.Maps.DataProfile)));

            return services.BuildServiceProvider();
        }

    }
}
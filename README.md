# Dominoes

## Introduction
This is a side project on learning new technology and building a multiplayer Dominoes game.

I've been working on many exciting technology in my professional life; but as an Architect I haven't been able to get as in depth as I would like. My primary focus are the following. 

* .NET Core
* Micro-services
* Kubernetes
* Kafka or other stream processing technology

This repository will contain my progress notes, best practices, and other tidbits. This is mostly for my learning but it's open to everyone.

## Links
[General Setup](docs/setup.md)


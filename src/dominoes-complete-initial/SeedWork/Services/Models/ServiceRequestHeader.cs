﻿namespace Hqv.Dominoes.Initial.Domain.Services.Models
{
    public class ServiceRequestHeader
    {
        public ServiceRequestHeader(string correlationId)
        {
            CorrelationId = correlationId;
        }

        public string CorrelationId { get; }
    }
}

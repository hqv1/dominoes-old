﻿using AutoMapper;
using Hqv.Dominoes.Initial.Domain.Models;
using Hqv.Dominoes.WebApi.Models;

namespace Hqv.Dominoes.WebApi.Maps
{
    public class ModelProfile : Profile
    {
        public ModelProfile()
        {
            CreateMap<AddPlayerModel, Player>()
                .ReverseMap();
        }
    }
}

﻿using CommandLine;
using Hqv.Dominoes.Hosts;
using Hqv.Dominoes.Initial.ConsoleHost.ConsoleOptions;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hqv.Dominoes.Initial.ConsoleHost
{
    internal static class Program
    {
        private static readonly IConfigurationRoot _configuration;
        private static readonly IServiceProvider _services;

        static Program()
        {
            _configuration = MyConfigurationBuilder.GetConfiguration();
            _services = Ioc.RegisterComponents(_configuration);
        }

        private static void Main(string[] args)
        {
            var mediator = _services.GetService<IMediator>();
            try
            {
                var result = Parser.Default.ParseArguments<
                        NoOpConsoleOption,
                        AddPlayerConsoleOption>(args)
                    .WithParsed<IRequest>(opts => Send(mediator, opts).Wait())
                    .WithNotParsed(errs => HandleParseError(errs, args));
            }
            catch (Exception ex)
            {
                var logger = _services.GetService<ILogger>();
                logger.Error(ex, "Fatal exception");
                Console.WriteLine($"Exception. See logs: {ex.Message}");
            }
        }

        private static int HandleParseError(IEnumerable<Error> errs, string[] args)
        {
            var logger = _services.GetService<ILogger>();
            var exception = new Exception("Unable to parse command");
            exception.Data["args"] = string.Join("; ", args) + " --- ";
            exception.Data["errors"] = string.Join("; ", errs.Select(x => x.Tag));
            logger.Error(exception, "Exiting programming");

            return 1;
        }

        private static async Task Send(IMediator mediator, IRequest opts)
        {
            await mediator.Send(opts).ConfigureAwait(false);
        }
    }
}
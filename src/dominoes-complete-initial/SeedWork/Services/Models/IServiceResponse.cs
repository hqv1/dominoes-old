﻿using Hqv.Dominoes.Initial.Domain.Exceptions;

namespace Hqv.Dominoes.Initial.Domain.Services.Models
{
    public interface IServiceResponse
    {
        public DominoException? Exception { get; }
    }
}

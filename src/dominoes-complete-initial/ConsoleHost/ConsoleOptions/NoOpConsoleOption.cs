﻿using CommandLine;
using MediatR;

namespace Hqv.Dominoes.Initial.ConsoleHost.ConsoleOptions
{
    [Verb("no-op", HelpText = "Only for demostration. Delete")]
    public class NoOpConsoleOption : IRequest
    {
    }
}

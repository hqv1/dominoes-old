﻿using Hqv.Dominoes.Initial.Core.Services;
using Hqv.Dominoes.Initial.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hqv.Dominoes.Initial.Domain.Services
{
    public interface IGame
    {
        Task<IList<Player>> GetPlayers();

        Task<Board> GetBoard();

        Task<Player> GetNextPlayer();

        Task<GameStatus> GetGameStatus();
    }
}
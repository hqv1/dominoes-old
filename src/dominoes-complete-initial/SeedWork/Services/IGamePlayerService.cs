﻿using Hqv.Dominoes.Initial.Core.Services;
using System.Threading.Tasks;

namespace Hqv.Dominoes.Initial.Domain.Services
{
    public interface IGamePlayerService
    {
        Task<Hand> GetHand();

        Task<int> GetScore();

        Task<AddDominoOntoBoardResponse> AddDominoOntoBoard(AddDominoOntoBoardRequest request);

        Task<PickDominoFromGraveyardResponse> PickDominoFromGraveyard(PickDominoFromGraveyardRequest request);
    }
}
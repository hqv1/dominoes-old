﻿using Hqv.Dominoes.Initial.Domain.Models;
using MediatR;

namespace Hqv.Dominoes.Initial.Domain.Services.Models
{
    public class AddPlayerRequest : IServiceRequest, IRequest<AddPlayerResponse>
    {
        public AddPlayerRequest(Player player)
        {
            Player = player;
        }

        public Player Player { get; }
    }
}
﻿using System;

namespace Hqv.Dominoes.Initial.Domain.Exceptions
{
    [Serializable]
    public class DominoException : Exception
    {
        public DominoException() { }
        public DominoException(string message) : base(message) { }
        public DominoException(string message, Exception inner) : base(message, inner) { }
        public DominoException(DominoExceptionType exceptionType, string message) : base(message) {
            ExceptionType = exceptionType;
        }

        public DominoException(DominoExceptionType exceptionType, string message, Exception inner) : base(message, inner) {
            ExceptionType = exceptionType;
        }

        protected DominoException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }

        public DominoExceptionType ExceptionType { get; }
    }
}
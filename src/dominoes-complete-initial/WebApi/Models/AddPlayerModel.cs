﻿namespace Hqv.Dominoes.WebApi.Models
{
    public class AddPlayerModel
    {
        public string? Nickname { get; set; }
    }
}

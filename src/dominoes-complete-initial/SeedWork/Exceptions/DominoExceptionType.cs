﻿using FluentValidation;
using System;

namespace Hqv.Dominoes.Initial.Domain.Exceptions
{

    public enum DominoExceptionType
    {
        Unknown,
        DomainValidation,
        DomainLogic
    }
}
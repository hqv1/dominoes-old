# Laptop Setup

## General Tools
I have a Windows laptop. These are the development tools I have installed.

[Chocolatey](https://chocolatey.org/) is a package manager for window. I use it to install other packages.

[Visual Studio](https://visualstudio.microsoft.com/) and [Visual Studio Code](https://code.visualstudio.com/) are my IDEs


## Misc
I set up my repository based on [Repository structure](https://github.com/kriasoft/Folder-Structure-Conventions).


﻿using AutoMapper;
using Hqv.Dominoes.Initial.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System.Reflection;

namespace Hqv.Dominoes.Hosts
{
    public static class MyServiceBuilder
    {
        public static void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            var logger = MyConfigurationBuilder.GetLogger(configuration);
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(logger, dispose: true));

            services.AddOptions();

            services.AddDbContext<DominoesDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("dominoes")));

            services.AddMediatR(Assembly.GetEntryAssembly(), Assembly.GetAssembly(typeof(Initial.Core.Handlers.AddPlayerHandler)));

            services.AddAutoMapper(Assembly.GetEntryAssembly(), Assembly.GetAssembly(typeof(Initial.Core.Maps.DataProfile)));
        }
    }
}

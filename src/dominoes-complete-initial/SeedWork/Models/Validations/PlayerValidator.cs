﻿using FluentValidation;
using Hqv.Dominoes.Initial.Domain.Models;

namespace Hqv.Dominoes.Initial.Core.Models
{

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1710:Identifiers should have correct suffix", Justification = "Correct suffix")]
    public class PlayerValidator : AbstractValidator<Player>
    {
        public PlayerValidator()
        {
            RuleFor(x => x.Id).GreaterThanOrEqualTo(1);
            RuleFor(x => x.Nickname).NotEmpty();
        }
    }
}
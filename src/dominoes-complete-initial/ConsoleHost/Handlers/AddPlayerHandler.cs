﻿using Hqv.Dominoes.Initial.ConsoleHost.ConsoleOptions;
using Hqv.Dominoes.Initial.Domain.Services.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Hqv.Dominoes.Initial.ConsoleHost.Handlers
{
    public class AddPlayerHandler : IRequestHandler<AddPlayerConsoleOption>
    {
        private readonly ILogger logger;
        private readonly IMediator mediator;

        public AddPlayerHandler(ILogger<AddPlayerHandler> logger, IMediator mediator)
        {
            this.logger = logger;
            this.mediator = mediator;
        }

        public async Task<Unit> Handle(AddPlayerConsoleOption request, CancellationToken cancellationToken)
        {
            //todo: Use FluentValidation
            if (string.IsNullOrEmpty(request.Nickname))
            {
                logger.LogError("Nickname cannot be null");
                Console.WriteLine("Validation error");
                return new Unit();
            }

            //todo: Use mapper to map from AddPlayerConsoleOption to Player.
            var addPlayerRequest = new AddPlayerRequest(new Domain.Models.Player(request.Nickname));

            var response = await mediator.Send(addPlayerRequest, cancellationToken).ConfigureAwait(false);

            if(response.Exception == null)
            {
                logger.LogInformation("Player {nickname} added with id {id}", response.Player.Nickname, response.Player.Id);
            }
            else
            {
                logger.LogError(response.Exception, "Adding player {nickname} failed.", response.Player.Nickname);
            }

            return new Unit();
        }
    }
}

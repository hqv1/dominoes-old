﻿namespace Hqv.Dominoes.Initial.Domain.Models
{
    public class Player
    {
        public Player(long id, string nickname)
        {
            Id = id;
            Nickname = nickname;
        }

        public Player(string nickname)
        {
            Nickname = nickname;
        }

        public long Id { get; }
        public string Nickname { get; }
    }
}
﻿using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Hqv.Dominoes.Initial.Core.Handlers;
using Hqv.Dominoes.Initial.Core.Maps;
using Hqv.Dominoes.Initial.Data;
using Hqv.Dominoes.Initial.Domain.Models;
using Hqv.Dominoes.Initial.Domain.Services.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Hqv.Dominoes.Initial.Core.Test
{
    public class AddPlayerHandlerTests
    {
        private readonly AddPlayerHandler handler;

        private readonly Mock<ILogger<AddPlayerHandler>> logger;
        private readonly IMapper mapper;
        private readonly DominoesDbContext dbContext;

        private Player requestedPlayer;
        private AddPlayerResponse response;

        private readonly Fixture fixture;

        public AddPlayerHandlerTests()
        {
            fixture = new Fixture();

            logger = new Mock<ILogger<AddPlayerHandler>>();

            var config = new MapperConfiguration(cfg => cfg.AddProfile<DataProfile>());
            mapper = config.CreateMapper();

            var dbOptions = new DbContextOptionsBuilder<DominoesDbContext>()
                .UseInMemoryDatabase(databaseName: "Dominoes")
                .Options;
            dbContext = new DominoesDbContext(dbOptions);

            handler = new AddPlayerHandler(dbContext, logger.Object, mapper);
        }

        [Fact]
        public async Task Should_InsertPlayer()
        {
            GivenNewPlayer();
            await WhenAddPlayerIsCalled().ConfigureAwait(false);
            ThenResponseIsSuccessful();
            ThenPlayerIsAdded();
        }

        private void GivenNewPlayer()
        {
            requestedPlayer = fixture.Create<Player>();
        }

        private async Task WhenAddPlayerIsCalled()
        {
            var request = new AddPlayerRequest(requestedPlayer);
            response = await handler.Handle(request, new System.Threading.CancellationToken()).ConfigureAwait(false);
        }

        private void ThenResponseIsSuccessful()
        {
            response.Exception.Should().BeNull();
        }

        private void ThenPlayerIsAdded()
        {
            dbContext.Players.Any(x => x.Nickname == requestedPlayer.Nickname).Should().BeTrue();
        }
    }
}
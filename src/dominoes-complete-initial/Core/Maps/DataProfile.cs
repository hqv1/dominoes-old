﻿using AutoMapper;

namespace Hqv.Dominoes.Initial.Core.Maps
{
    public class DataProfile : Profile
    {
        public DataProfile()
        {
            CreateMap<Domain.Models.Player, Data.Models.Player>()
                .ReverseMap();
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Hqv.Dominoes.Initial.Data;
using Hqv.Dominoes.Initial.Domain.Models;
using Hqv.Dominoes.Initial.Domain.Services.Models;
using Hqv.Dominoes.WebApi.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private readonly ILogger<PlayersController> logger;
        private readonly IMediator mediator;
        private readonly DominoesDbContext dbContext;
        private readonly IMapper mapper;

        public PlayersController(DominoesDbContext dbContext, ILogger<PlayersController> logger, IMapper mapper, IMediator mediator)
        {
            this.logger = logger;
            this.mediator = mediator;
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<Player>> CreateAsync(AddPlayerModel model)
        {
            using (logger.BeginScope(new Dictionary<string, object> { ["nickName"] = model.Nickname ?? string.Empty }))
            {
                var player = mapper.Map<Player>(model);
                var request = new AddPlayerRequest(player);
                var response = await mediator.Send(request).ConfigureAwait(false);

                if (response.Exception != null)
                {
                    return Problem();
                }

                return CreatedAtAction(nameof(Get), new { id = response.Player.Id }, response.Player);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Player>> Get(long id)
        {
            using(logger.BeginScope(new Dictionary<string, object> { ["playerId"] = id }))
            {
                logger.LogDebug("Getting player");
                var dbPlayer = await dbContext.Players.FindAsync(id).ConfigureAwait(false);
                if (dbPlayer == null)
                {
                    logger.LogDebug("Player not found");
                    return NotFound();
                }

                var player = mapper.Map<Player>(dbPlayer);
                logger.LogDebug("Player found");
                return player;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hqv.Dominoes.WebApi
{
    public static class ApiHeaders
    {
        public const string CorrelationId = "Correlation-Id";
    }
}

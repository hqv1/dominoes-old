﻿using System.ComponentModel.DataAnnotations;

namespace Hqv.Dominoes.Initial.Data.Models
{
    public class Player
    {
        public Player(long id, string nickname)
        {
            Id = id;
            Nickname = nickname;
        }

        public long Id { get;}
        public string Nickname { get; }
    }
}
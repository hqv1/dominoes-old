﻿using AutoMapper;
using FluentValidation;
using Hqv.Dominoes.Initial.Data;
using Hqv.Dominoes.Initial.Domain.Exceptions;
using Hqv.Dominoes.Initial.Domain.Models.Validations;
using Hqv.Dominoes.Initial.Domain.Services.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Npgsql;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Hqv.Dominoes.Initial.Core.Handlers
{
    public class AddPlayerHandler : IRequestHandler<AddPlayerRequest, AddPlayerResponse>
    {
        private readonly DominoesDbContext dbContext;
        private readonly ILogger logger;
        private readonly IMapper mapper;

        public AddPlayerHandler(DominoesDbContext dbContext, ILogger<AddPlayerHandler> logger, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.logger = logger;
            this.mapper = mapper;
        }

        public async Task<AddPlayerResponse> Handle(AddPlayerRequest request, CancellationToken cancellationToken)
        {
            try
            {
                logger.LogDebug("Adding player");

                Validate(request);
                var insertedPlayer = await InsertIntoDatabase(request, cancellationToken).ConfigureAwait(false);
                var playerResponse = mapper.Map<Domain.Models.Player>(insertedPlayer);
                logger.LogDebug("Player added with id {Id}", playerResponse.Id);
                return new AddPlayerResponse(playerResponse);
            }
            catch (DominoException ex)
            {
                return new AddPlayerResponse(request.Player, ex);
            }
            catch (Exception ex)
            {
                var exception = CreateAndLogDominoException(DominoExceptionType.Unknown, $"Uncaught exception in Handler", ex);
                return new AddPlayerResponse(request.Player, exception);
            }
        }

        private DominoException CreateAndLogDominoException(DominoExceptionType exceptionType, string exceptionMessage, Exception? ex = null)
        {
            var exception = ex == null ? new DominoException(exceptionType, exceptionMessage) : new DominoException(exceptionType, exceptionMessage, ex);
            logger.LogInformation(exception, "Adding player failed");
            return exception;
        }

        private void Validate(AddPlayerRequest request)
        {
            try
            {
                logger.LogDebug("Validating player");
                var validator = new PlayerToInsertValidator();
                validator.ValidateAndThrow(request.Player);
                logger.LogDebug("Validating player completed");
            }
            catch (Exception ex)
            {
                throw CreateAndLogDominoException(DominoExceptionType.DomainValidation, $"Validate {nameof(request)}", ex);
            }
        }

        private async Task<Data.Models.Player> InsertIntoDatabase(AddPlayerRequest request, CancellationToken cancellationToken)
        {
            logger.LogDebug("Adding player into database");
            var model = mapper.Map<Data.Models.Player>(request.Player);

            try
            {
                dbContext.Players.Add(model);
                await dbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (DbUpdateException ex) when (ex.InnerException is PostgresException && ((PostgresException)ex.InnerException).SqlState == "23505")
            {
                throw CreateAndLogDominoException(DominoExceptionType.DomainValidation, "Duplicate key in database", ex);
            }
            catch(Exception ex)
            {
                throw CreateAndLogDominoException(DominoExceptionType.Unknown, "Insert into database", ex);
            }

            logger.LogDebug("Player added into database");
            return model;
        }
    }
}

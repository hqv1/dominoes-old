﻿using Hqv.Dominoes.Initial.Core;
using Hqv.Dominoes.Initial.Core.Services;
using Hqv.Dominoes.Initial.Domain.Services.Models;
using System.Threading.Tasks;

namespace Hqv.Dominoes.Initial.Domain.Services
{
    public interface IDominoService
    {
        Task<AddPlayerResponse> AddPlayer(AddPlayerRequest request);

        Task<StartGameResponse> StartGame(StartGameRequest request);

        Task<Game> GetGame(int gameId);
    }
}
﻿using FluentValidation;

namespace Hqv.Dominoes.Initial.Domain.Models.Validations
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1710:Identifiers should have correct suffix", Justification = "Correct suffix")]
    public class PlayerToInsertValidator : AbstractValidator<Player>
    {
        public PlayerToInsertValidator()
        {
            RuleFor(x => x.Id).Equals(0);
            RuleFor(x => x.Nickname).NotEmpty();
        }
    }
}

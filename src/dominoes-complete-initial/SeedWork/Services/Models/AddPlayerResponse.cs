﻿using Hqv.Dominoes.Initial.Domain.Exceptions;
using Hqv.Dominoes.Initial.Domain.Models;

namespace Hqv.Dominoes.Initial.Domain.Services.Models
{
    public class AddPlayerResponse : IServiceResponse
    {
        public AddPlayerResponse(Player player)
        {
            Player = player;
        }

        public AddPlayerResponse(Player player, DominoException exception)
        {
            Player = player;
            Exception = exception;
        }

        public Player Player { get; }
        public DominoException? Exception { get; }
    }
}
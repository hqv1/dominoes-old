﻿using CommandLine;
using MediatR;

namespace Hqv.Dominoes.Initial.ConsoleHost.ConsoleOptions
{

    [Verb("add-player", HelpText = "Add player")]
    public class AddPlayerConsoleOption : IRequest
    {
        [Option('n', "nickname", Required = true)]
        public string? Nickname { get; set; }
    }
}
